package networkname

const (
	MainnetChainName             = "mainnet"
	HoleskyChainName             = "holesky"
	SepoliaChainName             = "sepolia"
	GoerliChainName              = "goerli"
	DevChainName                 = "dev"
	MumbaiChainName              = "mumbai"
	AmoyChainName                = "amoy"
	BorMainnetChainName          = "bor-mainnet"
	BorDevnetChainName           = "bor-devnet"
	GnosisChainName              = "gnosis"
	BorE2ETestChain2ValName      = "bor-e2e-test-2Val"
	ChiadoChainName              = "chiado"
	PulsechainChainName          = "pulsechain"
	PulsechainDevnetChainName    = "pulsechain-devnet"
	PulsechainTestnetV4ChainName = "pulsechain-testnet-v4"
)

var All = []string{
	MainnetChainName,
	HoleskyChainName,
	SepoliaChainName,
	GoerliChainName,
	MumbaiChainName,
	AmoyChainName,
	BorMainnetChainName,
	BorDevnetChainName,
	GnosisChainName,
	ChiadoChainName,
	PulsechainChainName,
	PulsechainDevnetChainName,
	PulsechainTestnetV4ChainName,
}
